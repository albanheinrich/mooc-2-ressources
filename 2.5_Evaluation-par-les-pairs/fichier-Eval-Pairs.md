**Thème du programme NSI choisi :** ###

1. **fiche prof** : _lien type = https://gitlab.com/#####/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche-prof.md_<br/>
si besoin fiche élève : _lien_

2. **analyse d'intention** de l'activité élève : _lien type = https://gitlab.com/#####/mooc-2-ressources/-/blob/main/2_Mettre-en-oeuvre-Animer/activite.md_

3. une remédiation pour élèves en difficulté : _lien_ <br/>
**ou** une activité d'approfondissement : _lien_ <br/>
**ou** un mini projet : _lien_<br/>
!!! préciser le contexte et la nature de votre activité !!!

4. Evaluation 
- Si pour le point 3, vous avez choisi le mini projet, vous devrez proposer ici une **grille d'évaluation** adaptée : _lien_
- Sinon, proposer une évaluation de type **devoir sur table** : _lien_
